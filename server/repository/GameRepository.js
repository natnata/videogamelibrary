const _ = require('lodash');
const Game = require('../models/game');

const GameRepository = {

  getGames(search) {
    let reg = null;
    try {
      reg = search ? new RegExp(search, 'i') : '';
    } catch (e) {
      return new Promise(function (resolve, reject) {
        reject({ name: 'patternError' });
      });
    }
    return Game.find({ name: { $regex: reg } });
  },

  getGame(id) {
    return Game.findOne({ _id: id });
  },

  createGame(gameData) {
    const newGame = new Game(gameData);
    return newGame.save();
  },

  updateGame(id, gameData) {
    return Game.findOne({ _id: id })
    .then(function (game) {
      if (!game) {
        return new Promise(function (resolve, reject) {
          reject({ name: 'CastError' });
        });
      }
      _.assign(game, gameData);
      return game.save();
    });
  },

  deleteGame(id) {
    return Game.findOne({ _id: id })
    .then(function (game) {
      if (!game) {
        return new Promise(function (resolve, reject) {
          reject({ name: 'CastError' });
        });
      }
      return game.remove();
    });
  },
};

module.exports = GameRepository;
