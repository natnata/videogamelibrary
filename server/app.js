const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/routes'); // Routes are defined here

const app = express(); // Create the Express app
require('./base/dbManager');

// Configure body-parser (middleware)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use('/api', routes); // This is the route middleware

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('./client/build'));
}

app.use(function(req, res) {
  res.status(404).send('Sorry, route not found.');
});

app.use(function(err, req, res) {
  res.status(500).send('Something broke!');
});

module.exports = app;
