const GamesController = require('../controllers/gamesController');
const express = require('express');

const router = express.Router();

router.route('/games')
  .get(GamesController.getGames)
  .post(GamesController.createGame);

router.route('/games/:id')
  .put(GamesController.updateGame)
  .get(GamesController.getGame)
  .delete(GamesController.deleteGame);

module.exports = router;
