const mongoose = require('mongoose');
const Promise = require('bluebird');

mongoose.Promise = Promise;
const Schema = mongoose.Schema;

// Schema
const gameSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  year: {
    type: Number,
    required: true,
  },
  platform: String,
  description: String,
});

const Game = mongoose.model('Game', gameSchema);

module.exports = Game;
