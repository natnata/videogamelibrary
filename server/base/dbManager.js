var mongoose = require('mongoose');

// Connect to the database

let connectionString = process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/gameDB';

if (process.env.NODE_ENV === 'test') {
  connectionString = `mongodb://localhost:27017/gameDBTest`;
}

// Create the database connection
mongoose.connect(connectionString)

// CONNECTION EVENTS

// If the connection throws an error
mongoose.connection.on('error',function (err) {
  console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});