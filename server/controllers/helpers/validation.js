const _ = require('lodash');

// Schema

const Ajv = require('ajv');
const ajv = new Ajv({allErrors: true});

ajv.addKeyword('range', { compile: function (sch) {
  const min = sch[0];
  const max = sch[1];

  return function (data) { return data >= min && data <= max; }
} });

const schema = {
  properties: {
    name: {type: "string"},
    year: {range: [1950, 2059]},
    platform: {type: "string"}
  }
};

const schemaId = {
  required: ["id"]
}

const schemaNew = _.assign({}, schema, {required: ["name","year","platform"]});
const validateNew = ajv.compile(schemaNew);
const validateEdit = ajv.compile(schema);
const validateId = ajv.compile(schemaId);

// Validation

let validation = {
  
  validateNewGame(data) {
    const res = {
      valid: true,
      message: null,
    };

    const valid = validateNew(data);
    if (!valid) {
      res.valid = false;
      res.message = ajv.errorsText(validateNew.errors);
    }
    return res;
  },

  validateEditGame(data) {
    const res = {
      valid: true,
      message: null,
    };

    const valid = validateEdit(data);
    if (!valid) {
      res.valid = false;
      res.message = ajv.errorsText(validateEdit.errors);
    }
    return res;
  },

  validateId(data) {
    const res = {
      valid: true,
      message: null,
    };

    const valid = validateId(data);
    if (!valid) {
      res.valid = false;
      res.message = ajv.errorsText(validateEdit.errors);
    }
    return res;
  }
};

module.exports = validation;
