const GameRepository = require('../repository/GameRepository');
const validation = require('./helpers/validation');
const _ = require('lodash');



const gamesController = {
  getGames(req, res) {
    const nameSearched = req.query ? req.query.name : null;
    GameRepository.getGames(nameSearched)
    .then(function(games) {
      return res.status(200).send(games);
    })
    .catch(function (err) {
      if (err.name === 'patternError') {
        res.status(400).send({ error: 'Search is not a valid pattern.' });
      } else {
        res.status(500).send({ error: err });
      }
    });
  },

  getGame(req, res) {
    const id = req.params.id;

    GameRepository.getGame(id)
    .then(function (game) {
      return game ? res.status(200).send(game) : res.status(404).send({ error: 'There is no game with this id.' });
    })
    .catch(function (err) {
      if (err.name === 'CastError') {
        res.status(404).send({ error: 'There is no coincidence with this id.' });
      } else {
        res.status(500).send({ error: 'There is something wrong.' });
      }
    });
  },

  doGameCreate(game) {
    return GameRepository.createGame(game)
    .then(function (game) {
      return game;
    })
  },

  createGame(req, res) {
    const resultValidation = validation.validateNewGame(req.body);
    if (!resultValidation.valid) {
      return res.status(400).send({ error: { name: 'validation', message: resultValidation.message } });
    }
    const { name, year, platform, description } = req.body;

    gamesController.doGameCreate({ name, year, platform, description })
    .then(function (createdGame) {
      return res.status(200).send(createdGame);
    }).catch(function (err) {
      if (err.name === 'ValidationError') {
        res.status(400).send({ error: 'There is something wrong with game validation. Please, check input parameters.' });
      } else {
        res.status(500).send({ error: 'Oops, there was a problem creating the game' });
      }
    })
  },

  updateGame(req, res) {
    const resultValidation = validation.validateEditGame(req.body);
    if (!resultValidation.valid) {
      return res.status(400).send({ error: { name: 'validation', message: resultValidation.message } });
    }
    const id = req.params.id;
    const { name, year, platform, description } = req.body;
    const game = _.pickBy({ name, year, platform, description }, value => (value === '' || !!value ));

    GameRepository.updateGame(id, game)
    .then(function (game) {
      return res.status(200).send(game);
    })
    .catch(function (err) {
      if (err.name === 'CastError') {
        res.status(404).send({ error: 'There is no coincidence with this id.' });
      } else {
        res.status(500).send({ error: 'Oops, there was a problem updating the game' });
      }
    });
  },

  deleteGame(req, res) {
    const id = req.params.id;
    const resultValidation = validation.validateId(id);
    if (!resultValidation.valid) {
      return res.status(400).send({ error: { name: 'validation', message: resultValidation.message } });
    }
    GameRepository.deleteGame(id)
    .then(function(game) {
      res.status(200).send(game);
    })
    .catch(function (err) {
      if (err.name === 'CastError') {
        res.status(404).send({ error: 'There is no coincidence with this id.' });
      } else {
        res.status(500).send({ error: 'Oops, there was a problem deleting the game' });
      }
    });
  },
};

module.exports = {
  getGame,
  getGames,
  createGame,
  updateGame,
  deleteGame,
} = gamesController;
