const app = require('../app');
const Game = require('../models/game');
const mongoose = require('mongoose');
const Promise = require('bluebird');

// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');

const expect = chai.expect;

chai.use(chaiHttp);

describe('APIRest', () => {

    const idOne = mongoose.Types.ObjectId('000000000000000000000001');
    const idTwo = mongoose.Types.ObjectId('000000000000000000000002');
    const idThree = mongoose.Types.ObjectId('000000000000000000000003');

    const gameOne = {
        _id: idOne,
        name: "Just Dance",
        year: "2017",
        platform: "Nintendo Switch",
        description: "Dance!"
    },
    gameTwo = {
        _id: idTwo,
        name: "Zelda",
        year: "2017",
        platform: "Nintendo Switch",
        description: "Awesome"
    },
    gameThree = {
        _id: idThree,
        name: "Mario",
        year: "2017",
        platform: "Nintendo DS",
        description: "Great!"
    };

    before((done) => {
        Promise.join(
            Game.create(gameOne),
            Game.create(gameTwo),
            Game.create(gameThree)
        ).then((res) => {
            console.log('Three games added');
            done();
        })
        .catch((err) => {
            console.log(err);
            done();
        })
    });

    after((done) => {
        Game.remove({}, (err) => {
           done();
        });
    });
/*
  * Test the /GET route
  */
  describe('Tests games', () => {
      it('it should ADD a new game', (done) => {
        const game = {
            name: "Zelda",
            year: "2017",
            platform: "Nintendo Switch",
            description: "Awesome"
        };

        chai.request(app)
            .post('/api/games')
            .send(game)
            .end((err, res) => {
                expect(res).have.status(200);
                expect(res.body).be.a('object');
                expect(res.body.name).be.a('string');
                expect(res.body.year).be.a('number');
                expect(res.body.platform).be.a('string');
                done();
            });
      });

      it('it should return an error when ADD a new game without all required fields', (done) => {
        const game = {
            name: "Splatoon",
            year: "2015",
            description: "Awesome"
        };

        chai.request(app)
            .post('/api/games')
            .send(game)
            .end((err, res) => {
                expect(res).have.status(400);
                expect(res.res.statusMessage).to.include('Bad Request');
                done();
            });
      });

      it('it should GET a game', (done) => {

        chai.request(app)
        .get(`/api/games/${idOne}`)
        .end((err, resGet) => {
            expect(resGet.status).to.equal(200);
            expect(resGet.body.name).to.equal(gameOne.name);
            done();
        });
      });

      it('it should return an error when trying to GET a non existing game', (done) => {

        chai.request(app)
        .get('/api/games/100000000000000000000004')
        .end((err, resGet) => {
            expect(resGet.status).to.equal(404);
            expect(resGet.res.statusMessage).to.include('Not Found');
            done();
        });
      });

      it('it should EDIT a game', (done) => {
        const editField = {
            name: "Mario & Luigi",
        };

        chai.request(app)
        .put(`/api/games/${idThree}`)
        .send(editField)
        .end((err, resPut) => {
            expect(resPut.status).to.equal(200);
            expect(resPut.body.name).to.equal('Mario & Luigi');
            done();
        });
      });

      it('it should return an error when trying to EDIT a non existing game', (done) => {

          const editField = {
              name: "Mario & Luigi"
            };

            chai.request(app)
            .put('/api/games/100000000000000000000005')
            .send(editField)
            .end((err, resPut) => {
                expect(resPut.status).to.equal(404);
                expect(resPut.error.text).to.include('There is no coincidence with this id.');
                done();
            });
      });

      it('it should GET all the games', (done) => {
        chai.request(app)
            .get('/api/games')
            .end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body).be.a('array');
                expect(res.body.length).to.equal(4);
              done();
            });
      });

      it('it should return an error when the route is not valid', (done) => {
        chai.request(app)
            .get('/api/gamesAll')
            .end((err, res) => {
                expect(res.status).to.equal(404);
                expect(res.error.text).to.include('Sorry, route not found');
              done();
            });
      });

      it('it should DELETE a game', (done) => {

        chai.request(app)
        .delete(`/api/games/${idTwo}`)
        .end((err, resRmv) => {
            expect(resRmv.status).to.equal(200);
            expect(resRmv.body.name).to.be.equal('Zelda');
            done();
        });
    });

      it('it should return an error when trying to DELETE a non existing game', (done) => {

        chai.request(app)
                .delete('/api/games/100000000000000000000006')
                .end((err, resRmv) => {
                    expect(resRmv.status).to.equal(404);
                    expect(resRmv.error.text).to.include('There is no coincidence with this id.');
                    done();
                });
            });
      });
});

