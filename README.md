# Videogame list

Welcome! Nice to meet you :)

This is an application to take control on all your video games and platforms. I hope that you enjoy it.

Suggestions are welcome ;)

## Prerequisites

* Docker

## Installing

* Development:

  Download the code.

  Frontend:

    * Go to client folder and install dependencies with: `npm install`
    * Run the client: `npm run start`

  Backend:
    * Go to server folder and install dependencies with: `npm install`
    * Run the server: `npm run start`

  DB:
    * Install Mongo
    * Run Mongo: `mongod`

* Production:

  Download the code.

  Frontend:

    * Go to client folder and install dependencies with: `npm install`
    * Run the client:

      `docker-compose build web`

      `docker-compose up web`

  Backend & DB:

    * Go to server folder and install dependencies with: `npm install``
    * Run the server:

      `docker-compose build web`

      `docker-compose up web`

## Running the tests

Tests are done with Mocha and Chai. To run:

`npm runt test`

## Example

This project is deployed in Heroku. You can visit it here:

<http://videogamelist.herokuapp.com/>

## Built with

* React
* Node.js
* Express
* Mongoose
* MongoDB

## API examples


* GET

  * Get a game

    <http://videogamelist.herokuapp.com/api/games/[id_game]>

  * Get all games

    <http://videogamelist.herokuapp.com/api/games>

* POST

  * Create a game

    <http://videogamelist.herokuapp.com/api/games>

* PUT

  * Update a game

    <http://videogamelist.herokuapp.com/api/games/id_game]>

* DELETE

  * Remove a game

    <http://videogamelist.herokuapp.com/api/games/[id_game]>

## Authors

* Natalia Estévez
