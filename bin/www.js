// Dependencies
const app = require('../server/app'); // Require the app

// Get port from environment and store in Express
app.set('port', process.env.PORT || 5000);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});
