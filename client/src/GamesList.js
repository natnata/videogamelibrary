import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import GameListItem from './GameListItem';
import './style.css';

import { getRequest } from './Service';

import { Button, ListGroup, DropdownButton, ButtonToolbar, MenuItem } from 'react-bootstrap';

class GamesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      games: [],
      orderBy: 'nameAsc',
      search: '',
      error: null,
    };
    this.orderFilter = this.orderFilter.bind(this);
    this.orderGameList = this.orderGameList.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.handleResponse = this.handleResponse.bind(this);
  }

  componentDidMount() {
    const url = '/api/games';

    getRequest(url)
    .then(response => this.handleResponse(response))
    .then((games) => {
      const orderedGameList = this.orderFilter(games, this.state.orderBy);

      this.setState({ games: orderedGameList });
    })
    .catch(err => this.setState({ error: err }));
  }

  orderFilter(list, eventKey) {
    let orderFunction = null;
    let key = null;

    const orderASC = function(a, b) {
      const valueA = a[key].toLowerCase();
      const valueB = b[key].toLowerCase();
      return (valueA > valueB) ? 1 : ((valueB > valueA) ? -1 : 0);
    };
    const orderDESC = function(a, b) {
      const valueA = a[key].toLowerCase();
      const valueB = b[key].toLowerCase();
      return (valueA < valueB) ? 1 : ((valueB < valueA) ? -1 : 0);
    };

    if (eventKey === 'nameAsc') {
      orderFunction = orderASC;
      key = 'name';
    } else if (eventKey === 'nameDesc') {
      orderFunction = orderDESC;
      key = 'name';
    } else if (eventKey === 'yearAsc') {
      orderFunction = orderASC;
      key = 'year';
    } else if (eventKey === 'yearDesc') {
      orderFunction = orderDESC;
      key = 'year';
    } else {
      orderFunction = orderASC;
      key = 'name';
    }

    const orderedList = list.sort(orderFunction, key);

    return orderedList;
  }

  orderGameList(eventKey) {
    const orderedGameList = this.orderFilter(this.state.games, eventKey);
    this.setState({ games: orderedGameList, orderBy: eventKey });
  }

  handleResponse(response) {
    let error;
    if (response && !response.ok) {
      return response.json().then((err) => {
        error = new Error(err.error || 'Server error.');
        error.code = response.status;
        throw error;
      });
    }
    return response.json();
  }

  onSearch(ev) {
    const search = ev.target.value;
    const url = `/api/games?name=${search}`;

    getRequest(url)
    .then(response => this.handleResponse(response))
    .then((games) => {
      const orderedGameList = this.orderFilter(games, this.state.orderBy);

      this.setState({ games: orderedGameList, error: null })
    })
    .catch(err => this.setState({ error: err }))
  }

  render() {
    const url = '/games/new';
    const orderBy = this.state.orderBy;
    const gamesInfo = this.state.games.map((game, index) => <GameListItem key={index} data={game} />);
    const dropdown = (
      <DropdownButton bsStyle="default" title="Order by" id="dropdownFilter" onSelect={this.orderGameList} >
        <MenuItem eventKey="nameAsc" active={orderBy === "nameAsc"}> Name asc </MenuItem>
        <MenuItem eventKey="nameDesc" active={orderBy === "nameDesc"}> Name desc </MenuItem>
        <MenuItem eventKey="yearAsc" active={orderBy === "yearAsc"}> Year asc </MenuItem>
        <MenuItem eventKey="yearDesc" active={orderBy === "yearDesc"}> Year desc </MenuItem>
    </DropdownButton>
    );

    const error = (this.state.error && this.state.error.message) ? this.state.error.message : '';
    return (
    <div className="container">
      <legend>Games list</legend>
      <div className="navigation">
        <input className="filter" onChange={ this.onSearch } type="text" placeholder="Name" name="filter" />
        <Link to={url}><button className="buttonNew">+ New Game</button></Link>
        <ButtonToolbar className="dropdown">{ dropdown }</ButtonToolbar>
      </div>
      <div className="error"> {error} </div>
      <ul className="gameList">
        {gamesInfo}
      </ul>
      </div>
    );
  }
}

export default GamesList;
