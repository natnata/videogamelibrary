import React, { Component } from 'react';
import './App.css';
import Home from './Home.js';
import GamesList from './GamesList.js';
import GameDetails from './GameDetails.js';
import GameDetailsEdit from './GameDetailsEdit.js';
import GameDetailsNew from './GameDetailsNew.js';

import {
  BrowserRouter as Router,
  Route,
  HashRouter
} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <HashRouter>
        <div>
          <Route exact path='/' component={Home} />
          <Route exact path='/games' component={GamesList} />
          <Route exact path='/games/new' component={GameDetailsNew} />
          <Route exact path={/\/games\/([^new][\d\w]+)/} component={GameDetails} />
          <Route path='/games/:id/edit' component={GameDetailsEdit} />
        </div>
      </HashRouter>
    );
  }
}

export default App;
