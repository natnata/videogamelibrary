import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';

import { postRequest } from './Service';
import FormDetailsGame from './FormDetailsGame';

class GameDetailsNew extends Component {
  constructor(props) {
    super(props);
    this.state = {value: '', error: null};
  
    this.createGame = this.createGame.bind(this);
  }

  handleResponse(response) {
    let error;
    if (response && !response.ok) {
      return response.json().then((err) => {
        error = new Error(err.error || 'Server error.');
        error.code = response.status;
        throw error;
      });
    }
    return response.json();
  };

  createGame(name, year, platform, description) {
    const url = '/api/games';
    postRequest(url, { name, year, platform, description })
    .then(response => this.handleResponse(response))
    .then(response => {
      this.setState({ redirectToReferrer: true });
    })
    .catch(err => this.setState({ error: err }))
  }
    
  render() {
    const urlBack = `/games`;
    const { redirectToReferrer } = this.state;
    

    if (redirectToReferrer) {
      return (
        <Redirect to={ urlBack }/>
      )
    }

    return (<FormDetailsGame onSave={this.createGame}/>)
  }


}

export default GameDetailsNew;