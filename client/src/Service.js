const Service = {};

Service.getRequest = url => fetch(url);

Service.postRequest = (url, data) => {
  const request = new Request(url, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify(data),
  });
  return fetch(request);
};

Service.putRequest = (url, data) => {
  const request = new Request(url, {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify(data),
  });
  return fetch(request);
};

Service.deleteRequest = (url) => {
  const request = new Request(url, {
    method: 'DELETE',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
  });
  return fetch(request);
};

module.exports = Service;
