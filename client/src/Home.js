import React, { Component } from 'react';
import './Home.css';

import { Link } from 'react-router-dom';

class Home extends Component {
  render() {
    return (
      <div className="wrapper">
        <div className="header">
          <h2>Welcome to your video games list!</h2>
        </div>
        <div className="about">
          <p>This is an application to take control on all your video games and platforms. I hope that you enjoy it. <br/> Suggestions are welcome ;)</p>
          <Link to='/games'>
            <button> Let's play! </button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Home;
