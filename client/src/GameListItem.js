import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { ListGroupItem } from 'react-bootstrap';
import PropTypes from 'prop-types';

class GamesListItem extends Component {
  render() {
    const url = `/games/${this.props.data._id}`;
    return (
    <li className="gameDetail">
      <Link to={url}>
        <div>
          <div className="field"> {this.props.data.name}</div>
          <div>{this.props.data.platform}</div>
          <div>{this.props.data.year}</div>
        </div>
      </Link>
    </li>
    );
  }
}

GamesListItem.propTypes = {
  data: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    year: PropTypes.number,
    platform: PropTypes.string
  }),
};

export default GamesListItem;