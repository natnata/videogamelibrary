import React, { Component } from 'react';
import './style.css';
import PropTypes from 'prop-types';

import { getRequest, deleteRequest } from './Service';

import { Link } from 'react-router-dom';
import { Redirect} from 'react-router';

class GameDetails extends Component {
  constructor(props) {
    super(props);
    this.id = this.props.match.params[0];
    this.deleteGame = this.deleteGame.bind(this);
    this.state = {
      game: {
        id: '-',
        name: '-',
        year: '-',
        platform: '-',
        description: '-'
      },
      redirectToReferrer: false,
      error: null
    };
  }
  componentDidMount() {
    const url = `/api/games/${this.id}`;
    getRequest(url)
     .then(response => this.handleResponse(response))
    .then(game => this.setState({ game }))
    .catch(err => this.setState({ error: err }))
  }

  handleResponse(response) {
    let error;
    if (response && !response.ok) {
      return response.json().then((err) => {
        error = new Error(err.error || 'Server error.');
        error.code = response.status;
        throw error;
      });
    }
    return response.json();
  };

  deleteGame() {
    const url = `/api/games/${this.id}`;

    deleteRequest(url)
    .then(response => this.handleResponse(response))
    .then(response => this.setState({ redirectToReferrer: true }))
    .catch(err => this.setState({ error: err }))
  }
  render() {
    const url = `/games/${this.id}/edit`;
    const urlBack = '/games/';
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return (
        <Redirect to={'/games'}/>
      )
    }
    return (
      <div className="container">
        <legend>Game details</legend>
        <div className="navigation">
          <Link to={urlBack}><button className="buttonBack">Back </button></Link>
        </div>
        <ul className="flex-outer gameDetails">
          <li>
            <div className="field">Name</div>
            <div className="values">{this.state.game.name || '-'}</div>
          </li>
          <li>
            <div className="field">Year</div>
            <div className="values">{this.state.game.year || '-'}</div>
          </li>
          <li>
            <div className="field">Platform</div>
            <div className="values">{this.state.game.platform || '-'}</div>
          </li>
          <li>
            <div className="field">Description</div>
            <div className="values">{this.state.game.description || '-'}</div>
          </li>
          <li className="buttonForm">
            <button onClick={this.deleteGame} className="buttonRemove"> Remove game </button>
            <Link to={url}><button> Edit info </button></Link>
          </li>
        </ul>
      </div>
    );
  }
}

GameDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.array
  }),
};

export default GameDetails;
