import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';

import { Form, Text, Textarea } from 'react-form';

import PropTypes from 'prop-types';
import './style.css';

class FormDetailsGame extends Component {
  constructor(props) {
    super(props);
    this.isValidYear = this.isValidYear.bind(this);
    this.onNameChange = this.onNameChange.bind(this);
  }

  isValidYear(year) { return /^(19[5-9]\d|20[0-5]\d)$/.test(year); }

  onNameChange(e) {
    this.setState({ name: e.target.value });
  }

  render() {
    let title = 'New game';
    let urlBack = '/games/';
    let valuesGame = {
      name: '',
      year: '',
      platform: '',
      description: '',
    };
    if (this.props.data) {
      title = 'Edit game';
      urlBack = `/games/${this.props.data.game._id}`;
      valuesGame = this.props.data.game;
    }
    return (
    <Form values={valuesGame}
        onSubmit={(values) => {
          const {name, year, platform, description } = values;
          this.props.onSave(name, year, platform, description);
        }}

        validate={({ name, year, platform }) => {
          return {
            name: !name ? 'A name is required' : undefined,
            year: !this.isValidYear(year) ? 'The year must be between 1950 - 2059' : undefined,
            platform: !platform ? 'You must add the platform' : undefined
          }
        }}
      >
        {({submitForm}) => {
          return (
            <div className="container">
            <legend>{title}</legend>
            <div className="navigation">
              <Link to={urlBack}><button className="buttonBack">Back</button></Link>
            </div>
            <form onSubmit={submitForm}>
              <ul className="flex-outer detailsItems">
                <li>
                  <label for="name" className="field">Name </label>
                  <Text field="name" className="values" id="name" placeholder="Name"/>
                </li>
                <li>
                  <label for="year" className="field">Year </label>
                  <Text field="year" className="values" id="year" placeholder="Year"/>
                </li>
                <li>
                  <label for="platform" className="field">Platform </label>
                  <Text field="platform" className="values" id="platform" placeholder="Platform"/>
                </li>
                <li>
                  <label for="description" className="field">Description </label>
                  <Textarea field="description" className="values" id="description" placeholder="Description"/>
                </li>
                <li className="buttonForm">
                  <button type="submit">Save</button>
                </li>
              </ul>
            </form>
            </div>
          )
        }}
    </Form>
            
    );
  }
}

FormDetailsGame.propTypes = {
  onSave: PropTypes.func,
  data: PropTypes.shape({
    game: PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      year: PropTypes.number,
      platform: PropTypes.string,
      description: PropTypes.string
    })
  }),
};

export default FormDetailsGame;