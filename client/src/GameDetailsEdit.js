import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';

import { getRequest, putRequest } from './Service';
import FormDetailsGame from './FormDetailsGame';
import PropTypes from 'prop-types';

class GameDetailsEdit extends Component {
  constructor(props) {
    super(props);
    this.id = this.props.match.params.id;
    this.editGame = this.editGame.bind(this);
    this.setGameState = this.setGameState.bind(this);
    this.state = {
      game: {
        id: '-',
        name: '-',
        year: '-',
        platform: '-',
        description: ''
      },
      error: null
    }
  }

  componentDidMount() {
    const url = `/api/games/${this.props.match.params.id}`;

    getRequest(url)
    .then(response => this.handleResponse(response))
    .then(this.setGameState)
    .catch(err => this.setState({ error: err }))
  }

  setGameState(game) {
    this.setState({ game });
  }

  handleResponse(response) {
    let error;
    if (response && !response.ok) {
      return response.json().then((err) => {
        error = new Error(err.error || 'Server error.');
        error.code = response.status;
        throw error;
      });
    }
    return response.json();
  };

  editGame(name, year, platform, description) {
    const url = `/api/games/${this.id}`;

    putRequest(url, { name, year, platform, description })
    .then(response => this.handleResponse(response))
    .then(response => this.setState({ redirectToReferrer: true }))
    .catch(err => this.setState({ error: err }))
  }

  render() {
    const urlBack = `/games/${this.props.match.params.id}`;

    const { redirectToReferrer } = this.state;

      if (redirectToReferrer) {
      return (
        <Redirect to={ urlBack }/>
      )
    }

    return (
      (<FormDetailsGame onSave={this.editGame} data={this.state} />)
    );
  }
}

GameDetailsEdit.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  })
};

export default GameDetailsEdit;
